# UserApi by Adam Strzyżewski

## If you don't have the time to install

I have taken the liberty of hosting this api, you can skip to the Basic Idea... section 
of this readme, and follow the testing procedure at this link:

**http://quiz.astrzyzewski.pl** 

(I had to use an old subdomain due to some hosting issues)

## Install instruction

First step is to clone the repository into a folder of your server (local or development).

In a production setting, folders such as node_modules and vendor would be absent, but 
in this case for time saved they are included, which means that
the project is ready to go after cloning when it comes to dependencies.

**Required server:** Apache (tested XAMPP 3.3.2) (as interpretation of htaccess is necessary)

**PHP:** 7.2

Next you might want to add a virtual host for it, like so for XAMPP (**httpd-vhosts.conf**).

````
<VirtualHost *:80>
DocumentRoot "c:/xampp/htdocs/api"
ServerName api.local
<Directory "c:/xampp/htdocs/api">
		DirectoryIndex index.php
        AllowOverride All
        Require all granted
</Directory>
</VirtualHost>
````

And an appropriate line in **hosts** file.

````
127.0.0.1 api.local www.api.local
````

The entry point to the application is /public/index.php. 
Necessary instructions exists in .htaccess that redirect to it.
 
### Database Setup

**MYSQL**

Database create SQL code:

````
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;


-- Zrzut struktury bazy danych users
CREATE DATABASE IF NOT EXISTS `users` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `users`;
````

The above will create a database 'users' 
(you can change the name to any other)

Next you need to run this:

````
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `api_key` varchar(50) COLLATE utf8_bin DEFAULT '',
  `register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
````

In order to create a necessary table 'users' (this name is modifiable in Api.php in the 
$sTableName variable)

If you like to, you can optionally use this code to create a few example users:

````
INSERT INTO `users` (`email`, `name`, `password_hash`, `api_key`, `register_date`) VALUES ('elsa@gmail.com', 'Elsa', '$2y$10$hDJ4dl1DulbR/0l6Y3k3j.l5sh0QljXMUJBf99DQ2F0yu5zjS8rLS', '', '2018-11-14 08:57:19');
INSERT INTO `users` (`email`, `name`, `password_hash`, `api_key`, `register_date`) VALUES ('jean@gmail.com', 'Jean', '$2y$10$XXKYZ0.07bGINB.zefJPrOMXOvJDjoRg1wBlIkQAg95iVHEc5YD7O', '', '2018-11-14 08:57:28');
INSERT INTO `users` (`email`, `name`, `password_hash`, `api_key`, `register_date`) VALUES ('john@gmail.com', 'John', '$2y$10$pUMfzmdcQ1hmiUzIdyexgeFXG3oLAe08fiMBlv67HInDi2mWP1euu', '', '2018-11-14 08:57:50');
````

Password to all is '123'

Last step for the connection is configuring the connection, params are found in:

````
/api_path/config/db.config.php
````

With default values: 

````
    'driver' => 'mysqli',
    'database' => 'users',
    'host' => 'localhost',
    'username' => 'root',
    'password' => '',
````
Please modify database, host, username and password accordingly to your configuration.

If you have completed all the steps, upon navigating to api.local you should see the following:

````
{"sCode":"200","sMessage":"Welcome to UserApi"}
````

# Basic idea behind the interface

For this API the idea was quite simple, it is meant to fulfil the basic guide lines, be robust and cover a varied array of response, 
and add a few little touches to make it easier  to use (like it's documentation).

In addition to making sure the basics work, I have decided on adding one significant change 
(and in my mind an improvement). That change is the possibility of accessing most of API's 
functionality by both logging in, or an **API key**, generated for each user by one of the methods (see getApiKey).
This is a common idea in API interfaces all around, with Google`s Maps as first example that comes to mind.

A smaller, but still worth noting, addition to the baselines is the **/me** action, listing details about logged in user.
 
# A testing procedure that shows all possible methods

All links starting with /test are **case sensitive**, and lead to forms that 
create necessary POST requests leading to API`s actions.

## 1.Navigate to /testAdd 

You will see a form that allows to add a custom user, with an email, name and password that will
be stored as a password hash

For testing purposes, create a second user that you can delete 
(unless you've used the sql code for import)

## 2. Navigate to /testLogin

Enter credentials for a user existing in the database to log in.

## 3. Navigate to /me

This will show details of your logged-in-self

## 4. Navigate to /getApiKey 
(case insensitive)

From here you will receive you api key that can be used to access some of the methods

## 5 a. Navigate to /list

You will see a JSON response of a list of users

## 5 b. Navigate to /testList

If you put a correct API key into the form and submit it, you will receive the same result as 
/list when logged in

## 6 a. Navigate to /details/{id}, for example /details/1

If you are logged in, you will see details of a user of specified id.

## 6 b. Navigate to /testDetails

Enter you API key, and a user ID to see details of a user specified id.

## 7 a. Navigate to /delete/{id}, for example /delete/1

If you are a logged in, and you are not trying to delete yourself you will successfully delete
another user

## 7 b. Navigate to /testDeleteAPI

Enter you API key and a user id, it will delete him, 
unless that user id connected to the api key

## 7 a. Navigate to /testUpdateAPI

Using this form you are able to update your name and/or password
**Be careful,** updating password will affect your api key, so the old one might not work.
To see your new api key navigate to **/getApiKey**

## 7 b. Navigate to /testUpdateLogin

Using this form if you are logged in you are able to change your name and password

**Be careful,** if you change your password you will be logged out and your API key will change

## 8 Navigate to /logout

You have seen all methods acceptable by the API by now, 
and you can logout by accessing the given link


For specific on API`s methods and rules on building POST/GET requests neccessary to access them, 
please refer to the specification below

# API`s specification

## Allowed methods:

### Add user
**Link:** /add

**Method:** POST

**Test form api:** /testAdd

**Required data:**
````
$aParams = [
'sMethod' => 'add', //neccesary, otherwise will fail
'sPassword' =>  '123', //string of an original password
'sName' => 'John Doe', //name given by user
'sEmail' => 'test@test.pl' //a valid email address
]
````
**Return JSON**

In case of **fail**:
````
{"sCode":"Error Code(404, 405)","sMessage":"Error message"}
OR
{"aMissingParams":[Array of missing arguments],"sCode":"405","sMessage":"Missing Param"}
````
In case of **success**:
````
{"sCode":"200",
"iUserId":Added user id,
"sMessage":"Success Message"}
````
### Login user
**Link:** /login

**Method:** POST

**Test form:** /testLogin

**Required data:**
````
$aParams = [
'sMethod' => 'login', //neccesary, otherwise will fail
'sPassword' =>  '123', //string of an original password
'sEmail' => 'test@test.pl' //an email address
]
````
**Return JSON**

In case of **fail**:
````
{"sCode":"Error Code(404, 405)","sMessage":"Error message"}
OR
{"aMissingParams":[Array of missing arguments],"sCode":"405","sMessage":"Missing Param"}
````
In case of **success**:
````
{"sCode":"200","sMessage":"User correctly logged in"}
````

After a successful login two $_SESSION variables are created
$sEmail - email of logged in user
$iId - id of logged in user

### Logout user
**Link:** /logout

**Method:** GET

**Return JSON**

In case of **fail**:
````
{"sCode":"404","sMessage":"Nobody is logged in"}
````
In case of **success**:
````
{"sCode":"200","sMessage":"User logged out"}
````

Clears the session variables of a user which effectively logs them out

### getApiKey
**Link:** /getApiKey

**Method:** GET

**Return JSON**

In case of **fail**:
````
{"sCode":"405","sMessage":"User is not logged in"}
````
In case of **success**:
````
{"sCode":"200","sMessage":"MD5 generated API key"}
````

Creates and/or shows an API key which can be used for several methods as authentication.

If user has changed his/hers password it will generate a fresh API key

If key already was generated is lists it in the response 


### Update user
**Link:** /update

**Method:** POST

**Test form api:** /testUpdateAPI

**Test form login:** /testUpdateLogin

**Required data:**
````
$aParams = [
'sMethod' => 'update', //neccesary, otherwise will fail
'sAuth' => 'login' OR 'api', //neccesary, otherwise will fail
'sApiKey' => 'a correct api key connected to a user', //if api auth has been chosen
'sPassword' =>  '123', //optional
'sName' =>  '123', //optional
]
````
**Return JSON**

In case of **fail**:
````
{"sCode":"405","sMessage":"User not updated, wrong parameters"}
````
In case of **success**:
````
{"sCode":"200","sMessage":"User data updated"}
````

If method of authentification is "login" - API check if a user is actually logged in and performs an update.

If it's "api" - API checks if the key exists in a base and performs and update for a user row with that api.

If sName is different than the one in database it is updated to a new value.

Same check is performed for password hash based on the one in database

**Be careful, if password is updated, the api_key in database will be changed!**

### List users
**Link:** /list

**Method:** POST/GET

**Test form api:** /testList

**Test result get (must be logged in):** /list

**Required data for POST:**
````
$aParams = [
'sMethod' => 'list', //neccesary for POST, not GET
'sAuth' => 'api', //login is presumed with a GET method
'sApiKey' => 'a correct api key connected to a user', //if api auth has been chosen
]
````

**Required params for GET:**

````
NONE
````
Using GET (so just the link /list), will cause a check performed on $_SESSION, and if a user is logged in will return the same response

**Return JSON**

In case of **fail**:
````
{"sCode":"405","sMessage":"User not updated, wrong parameters"}
OR
{"sCode":"405","sMessage":"Api key not correct"}
````
In case of **success**:
````
{
    "sCode":"200",
    "aUserList":{
        "3":{
        "sUserEmail":"test@test.pl",
        "sUserName":"test23",
        "sUserRegisterDate":"2018-11-13 11:21:03"},
        
        "8":{
        "sUserEmail":"john@doe.pl",
        "sUserName":"John",
        "sUserRegisterDate":"2018-11-13 11:21:03"}
        }
    }
}
````

### Details of a user
**Link:** /details/{id} - {id} only required for GET request

**Method:** POST/GET

**Test form api:** /testDelete

**Required data for POST:**
````
$aParams = [
'sAuth' => 'api', //login is presumed with a GET method
'iUserId' => 'api', //neccesary with POST
'sMethod' => 'details', //neccesary for POST, not GET
'sApiKey' => 'a correct api key connected to a user', //if api auth has been chosen
]
````

**Required params for GET:**

````
NONE
````

Using GET (so just the link /list), will cause a check performed on $_SESSION, and if a user is logged in will return the same response

**Return JSON**

In case of **fail**:
````
{"sCode":"405","sMessage":"User not logged in"}
OR
{"sCode":"405","sMessage":"Api key not correct"}
OR
{"sCode":"404","sMessage":"User does not exist"}
````
In case of **success**:
````
{
"sCode":"200",
"aUserDetails":{
    "8":{
            "sUserEmail":"john@doe.pl",
            "sUserName":"John",
            "sUserRegisterDate":"2018-11-13 11:21:03"
        }
    }
}
````

### Delete a user
**Link:** /delete/{id} - {id} only required for GET request

**Method:** POST/GET

**Required data for POST:**
````
$aParams = [
'sAuth' => 'api', //login is presumed with a GET method
'sApiKey' => 'a correct api key connected to a user', //if api auth has been chosen
]
````

**Required params for GET:**

````
NONE
````

Using GET (so just the link /list), will cause a check performed on $_SESSION, and if a user is logged in will return the same response

**Return JSON**

In case of **fail**:
````
{"sCode":"404","sMessage":"User with that id does not exists"}
OR
{"sCode":"405","sMessage":"User not logged in"}
OR
{"sCode":"405","sMessage":"Api key not correct"}
OR
{"sCode":"404","sMessage":"User with that id does not exists or you are trying to delete yourself"}

````
In case of **success**:
````
{"sCode":"200","iDeletedUserId":"id of deleted user","sMessage":"User deleted"}
````

### Delete a user
**Link:** /me

**Method:** GET


**Required params for GET:**

````
NONE
````

If a user is logged in, this will return detail about him/her.

**Return JSON**

In case of **fail**:
````
{"sCode":"404","sMessage":"Nobody is logged in"}
````
In case of **success**:
````
{
"sCode":"200",
"aUserDetails":{
    "4":{
            "sUserEmail":"john.doe@gmail.com",
            "sUserName":"test",
            "sUserRegisterDate":"2018-11-13 15:50:56"
        }
    }
}
````