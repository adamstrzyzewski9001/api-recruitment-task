<?php


namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;


class Api
{
    protected $aDriver;
    protected $configArray;
    protected $oSql;
    protected $sTableName = 'users';

    public function __construct($aDb = [])
    {
        $this->saDriver = new Adapter($aDb);
        $this->oSql = new Sql($this->saDriver);
    }
    public function setTableName($sTableName)
    {
        $this->sTableName = $sTableName;
    }

    /*
     * Given an sql query it return a ResultSet with appropriate rows
     * if something with the connection or query went wrong returns false
     * */
    public function fetchAll($sWhere = null, $sOrd = null, $sLimit = null)
    {

        $sqlSelect = $this->oSql->select()
            ->from($this->sTableName);
        if ($sWhere) {
            $sqlSelect->where($sWhere);
        }
        if ($sOrd) {
            $sqlSelect->order($sOrd);
        }
        if ($sLimit) {
            $sqlSelect->limit($sLimit);
        }

        $statement = $this->oSql->prepareStatementForSqlObject($sqlSelect);
        $aResults = $statement->execute();
        $aResults->buffer();

        return $aResults;
    }

    public function fetchUserList()
    {
        $sqlSelect = $this->oSql->select()
            ->from($this->sTableName);


        $statement = $this->oSql->prepareStatementForSqlObject($sqlSelect);
        $aResults = $statement->execute();
        $aResults->buffer();

        $aReturn = [];
        foreach ($aResults as $aRow) {
            $aReturn[$aRow['id']] = [
                'sUserEmail' => $aRow['email'],
                'sUserName' => $aRow['name'],
                'sUserRegisterDate' => $aRow['register_date'],
            ];
        }
        return $aReturn;
    }

    public function fetchUser($id)
    {
        $sqlSelect = $this->oSql->select()
            ->from($this->sTableName)
            ->where("id = $id");


        $statement = $this->oSql->prepareStatementForSqlObject($sqlSelect);
        $aResults = $statement->execute();
        $aResults->buffer();

        $aReturn = [];
        foreach ($aResults as $aRow) {
            $aReturn[$aRow['id']] = [
                'sUserEmail' => $aRow['email'],
                'sUserName' => $aRow['name'],
                'sUserRegisterDate' => $aRow['register_date'],
            ];
        }
        return $aReturn;
    }

    public function removeUser($id)
    {
        $sqlSelect = $this->oSql->delete()
            ->from($this->sTableName)
            ->where("id = $id");

        $statement = $this->oSql->prepareStatementForSqlObject($sqlSelect);
        $aResults = $statement->execute();

        if ($aResults->getAffectedRows()) {
            return true;
        } else {
            return false;
        }

    }

    public function removeUserAPI($id, $sApiKey)
    {

        $sqlSelect = $this->oSql->delete()
            ->from($this->sTableName)
            ->where("id = $id AND api_key != '$sApiKey'");
        //->where("");

        $statement = $this->oSql->prepareStatementForSqlObject($sqlSelect);
        $aResults = $statement->execute();

        if ($aResults->getAffectedRows()) {
            return true;
        } else {
            return false;
        }

    }

    /*
     * Creates a user, returns array ['sErrorCode', 'iUserId']
     * */
    public function addUser($aParams)
    {

        $aParams['register_date'] = date("Y-m-d H:i:s");

        if ($this->fetchAll("email = '" . $aParams['email'] . "'")->getAffectedRows()) {
            return ['bError' => 0, 'iUserId' => null];
        } else {
            try {
                $sqlInsert = $this->oSql->insert($this->sTableName)
                    ->values($aParams);
                $statement = $this->oSql->prepareStatementForSqlObject($sqlInsert);
                $statement->execute();
                return ['bError' => 1, 'iUserId' => $this->oSql->getAdapter()->getDriver()->getLastGeneratedValue()];
            } catch (Exception $e) {
                return ['bError' => 0, 'iUserId' => null];
            }
        }
    }

    /*
   * Tries to login a user, returns array ['sErrorCode', 'sErrorMessage']
   * */
    public function loginUser($aParams)
    {

        $aParams['register_date'] = date("Y-m-d H:i:s");
        $oResult = $this->fetchAll("email = '" . $aParams['email'] . "'", null, 1)->current();
        if (!$oResult) {
            return ['bError' => 0, 'sErrorMessage' => "No user with that email persisted"];
        } else {
            if (password_verify($aParams['password'], $oResult['password_hash'])) {
                return ['bError' => 1, 'sErrorMessage' => "User logged in Correctly", 'iUserId' => $oResult['id']];
            } else {
                return ['bError' => 0, 'sErrorMessage' => "Incorrect Password"];
            }
        }

    }

    /*return api key after creating one if doesn't exists or updates if password changed*/
    public function setApiKey($sEmail, $iId)
    {
        $oResult = $this->fetchAll("email = '" . $sEmail . "'", null, 1)->current();
        if ($oResult['api_key']) {
            if ($oResult['api_key'] == md5($sEmail . $iId . $oResult['password_hash'])) {
                return $oResult['api_key'];
            } else {
                $sApiKey = md5($sEmail . $iId . $oResult['password_hash']);
                try {
                    $sqlInsert = $this->oSql->update($this->sTableName)
                        ->set(['api_key' => $sApiKey])
                        ->where(['id' => $iId]);
                    $statement = $this->oSql->prepareStatementForSqlObject($sqlInsert);
                    $statement->execute();
                    return $sApiKey;
                } catch (Exception $e) {
                    return false;
                }
            }
        } else {
            $sApiKey = md5($sEmail . $iId . $oResult['password_hash']);
            try {
                $sqlInsert = $this->oSql->update($this->sTableName)
                    ->set(['api_key' => $sApiKey])
                    ->where(['id' => $iId]);
                $statement = $this->oSql->prepareStatementForSqlObject($sqlInsert);
                $statement->execute();
                return $sApiKey;
            } catch (Exception $e) {
                return false;
            }
        }
    }

    /*Updates a user accordingly to data by id*/
    public function updateUserL($aParams, $iId)
    {
        $oResult = $this->fetchAll("id = '" . $iId . "'", null, 1)->current();
        if ($oResult) {
            $aValues = [];
            if (isset($aParams['sName'])) {
                if ($aParams['sName'] != "") {
                    if ($oResult['name'] != $aParams['sName']) {
                        $aValues['name'] = $aParams['sName'];
                    }
                }
            }
            if (isset($aParams['sPassword'])) {
                if ($aParams['sPassword'] != "") {
                    if (!password_verify($aParams['sPassword'], $oResult['password_hash'])) {
                        $aValues['password_hash'] = password_hash($aParams['sPassword'], PASSWORD_DEFAULT);
                        $this->setApiKey($oResult['email'], $oResult['id']);
                    }
                }
            }
            if (count($aValues) > 0) {
                try {
                    $sqlInsert = $this->oSql->update($this->sTableName)
                        ->set($aValues)
                        ->where(['id' => $iId]);
                    $statement = $this->oSql->prepareStatementForSqlObject($sqlInsert);
                    $statement->execute();
                    return true;
                } catch (Exception $e) {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    /*Updates a user accordingly to data by api key*/
    public function updateUserAPI($aParams)
    {
        if (!isset($aParams['sApiKey'])) {
            return false;
        }

        $oResult = $this->fetchAll("api_key = '" . $aParams['sApiKey'] . "'", null, 1)->current();
        if ($oResult) {
            $aValues = [];

            if (isset($aParams['sName'])) {
                if ($aParams['sName'] != "") {
                    if ($oResult['name'] != $aParams['sName']) {
                        $aValues['name'] = $aParams['sName'];

                    }
                }
            }
            if (isset($aParams['sPassword'])) {
                if ($aParams['sPassword'] != "") {
                    if (!password_verify($aParams['sPassword'], $oResult['password_hash'])) {
                        $aValues['password_hash'] = password_hash($aParams['sPassword'], PASSWORD_DEFAULT);
                        $this->setApiKey($oResult['email'], $oResult['id']);
                    }
                }
            }
            if (count($aValues) > 0) {
                try {
                    $sqlInsert = $this->oSql->update($this->sTableName)
                        ->set($aValues)
                        ->where(['api_key' => $aParams['sApiKey']]);
                    $statement = $this->oSql->prepareStatementForSqlObject($sqlInsert);
                    $statement->execute();
                    return true;
                } catch (Exception $e) {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }
}