<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Model\Api;


class IndexController extends AbstractActionController
{
    private $oModule;
    private $aValidateResponse;

    public function __construct()
    {
        include(__DIR__ . "/../../../../config/db.config.php");
        $this->oModule = new Api($aDbConfig);
        $this->aValidateResponse = [];
    }

    //main action, handles the sign in/register forms
    public function indexAction()
    {

        $aReturn['sCode'] = "200";
        $aReturn['sMessage'] = "Welcome to UserApi";
        return new JsonModel($aReturn);
    }

    /******************TEST FORM ACTIONS********************/

    /*Allows to test api-s login method with a form*/
    public function testLoginAction()
    {

    }

    public function testAddAction()
    {
    }

    public function testListAction()
    {
    }

    public function testDeleteAction()
    {
    }

    public function testDetailsAction()
    {
    }

    public function testUpdateLoginAction()
    {

    }

    public function testUpdateAPIAction()
    {

    }

    public function testDeleteAPIAction()
    {

    }

    public function testGetApiKey()
    {
        /*Testing function*/
        $sUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/getApiKey';
        $aData = array('sMethod' => 'login', 'sEmail' => "test@test.pl", 'sPassword' => '123');
        $aOptions = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($aData)
            )
        );
        $sccContext = stream_context_create($aOptions);
        $fgcResult = file_get_contents($sUrl, false, $sccContext);

        echo($fgcResult);
    }

    /***********************ACTIONS*************************/
    public function addAction()
    {
        $aReturn = [];
        //Check if request is POST to ensure security
        if ($this->getRequest()->isPost()) {
            //retrieve params
            $aPostParams = $this->params()->fromPost();
            $aRules = [
                "sEmail" => 'Email',
                "sPassword" => 'Password',
                "sName" => 'Name',
                "sMethod" => 'Method',
            ];
            $bMissingParam = $this->validate($aPostParams, $aRules);

            if (!$bMissingParam) {
                return new JsonModel($this->aValidateResponse);
            }

            $usEmail = $aPostParams['sEmail'];
            $usPassword = $aPostParams['sPassword'];
            $usName = $aPostParams['sName'];
            $usMethod = $aPostParams['sMethod'];

            if (!filter_var($usEmail, FILTER_VALIDATE_EMAIL)) {
                $aReturn['sCode'] = "405";
                $aReturn['sMessage'] = "Invalid Email";
                return new JsonModel($aReturn);
            }
            if (isset($usMethod)) {
                if ($usMethod == "add") {
                    $aParams = [];
                    $aParams['email'] = htmlspecialchars($usEmail);
                    $aParams['name'] = htmlspecialchars($usName);
                    $aParams['password_hash'] = password_hash($usPassword, PASSWORD_DEFAULT);

                    try {
                        $sResult = $this->oModule->addUser($aParams);
                        if ($sResult['bError']) {
                            $aReturn['sCode'] = "200";
                            $aReturn['iUserId'] = $sResult['iUserId'];
                            $aReturn['sMessage'] = "User added correctly";
                        } else {
                            $aReturn['sCode'] = "405";
                            $aReturn['sMessage'] = "User already exists";
                        }
                    } catch (Exception $e) {
                        $aReturn['sCode'] = "500";
                        $aReturn['sMessage'] = "Database Error, contact the administration";
                        exit;
                    }

                } else {
                    $aReturn['sCode'] = "405";
                    $aReturn['sMessage'] = "Wrong Method";
                }
            } else {
                $aReturn['sCode'] = "404";
                $aReturn['sMessage'] = "Undefined method";
            }

        } else {
            $aReturn['sCode'] = "404";
            $aReturn['sMessage'] = "Undefined method";
        }
        return new JsonModel($aReturn);
    }

    public function loginAction()
    {
        $aReturn = [];
        //Check if request is POST to ensure security

        if ($this->getRequest()->isPost()) {
            //retrieve params
            $aPostParams = $this->params()->fromPost();

            $aRules = [
                "sEmail" => 'Email',
                "sPassword" => 'Password',
                "sMethod" => 'Method',
            ];
            $bMissingParam = $this->validate($aPostParams, $aRules);

            if (!$bMissingParam) {
                return new JsonModel($this->aValidateResponse);
            }

            $usEmail = $aPostParams['sEmail'];
            $usPassword = $aPostParams['sPassword'];
            $usMethod = $aPostParams['sMethod'];

            if (!filter_var($usEmail, FILTER_VALIDATE_EMAIL)) {
                $aReturn['sCode'] = "405";
                $aReturn['sMessage'] = "Invalid Email";
                return new JsonModel($aReturn);
            }
            if (isset($usMethod)) {
                if ($usMethod == "login") {
                    $aParams = [];
                    $aParams['email'] = htmlspecialchars($usEmail);
                    $aParams['password'] = $usPassword;
                    try {
                        $sResult = $this->oModule->loginUser($aParams);
                        if ($sResult['bError']) {
                            $aReturn['sCode'] = "200";
                            $aReturn['sMessage'] = "User correctly logged in";
                            $aParams['id'] = $sResult['iUserId'];
                            $this->loginUser($aParams);
                        } else {
                            $aReturn['sCode'] = "405";
                            $aReturn['sMessage'] = "Login try denied";
                        }
                    } catch (Exception $e) {
                        $aReturn['sCode'] = "500";
                        $aReturn['sMessage'] = "Database Error, contact the administration";
                        exit;
                    }

                } else {
                    $aReturn['sCode'] = "405";
                    $aReturn['sMessage'] = "Wrong Method";
                }
            } else {
                $aReturn['sCode'] = "404";
                $aReturn['sMessage'] = "Undefined method";
            }

        } else {
            $aReturn['sCode'] = "404";
            $aReturn['sMessage'] = "Undefined method";
        }

        return new JsonModel($aReturn);

    }

    public function getApiKeyAction()
    {
        if (isset($_SESSION['sEmail'])) {
            if (isset($_SESSION['iId'])) {
                $sApiKey = $this->oModule->setApiKey($_SESSION['sEmail'], $_SESSION['iId']);
                $aReturn['sCode'] = "200";
                $aReturn['sMessage'] = $sApiKey;
            }
        } else {
            $aReturn['sCode'] = "405";
            $aReturn['sMessage'] = "User is not logged in";
        }
        return new JsonModel($aReturn);
    }

    public function logoutAction()
    {
        if (isset($_SESSION['sEmail'])) {
            if (isset($_SESSION['iId'])) {
                unset($_SESSION['sEmail']);
                unset($_SESSION['iId']);
                $aReturn['sCode'] = "200";
                $aReturn['sMessage'] = "User logged out";
            }
        } else {
            $aReturn['sCode'] = "404";
            $aReturn['sMessage'] = "Nobody is logged in";
        }
        return new JsonModel($aReturn);
    }

    public function meAction()
    {
        if (isset($_SESSION['sEmail'])) {
            if (isset($_SESSION['iId'])) {
                $aResult = $this->oModule->fetchUser($_SESSION['iId']);
                if ($aResult) {
                    $aReturn['sCode'] = "200";
                    $aReturn['aUserDetails'] = $aResult;
                } else {
                    $aReturn['sCode'] = "404";
                    $aReturn['sMessage'] = "User does not exist";
                }
            }
        } else {
            $aReturn['sCode'] = "404";
            $aReturn['sMessage'] = "Nobody is logged in";
        }
        return new JsonModel($aReturn);
    }

    public function updateAction()
    {
        $aReturn = [];

        //Check if request is POST to ensure security
        if ($this->getRequest()->isPost()) {
            //retrieve params
            $aPostParams = $this->params()->fromPost();

            $aRules = [
                "sAuth" => 'Authentification',
                "sMethod" => 'Method',
            ];

            $bMissingParam = $this->validate($aPostParams, $aRules);

            if (!$bMissingParam) {
                return new JsonModel($this->aValidateResponse);
            }

            $sAuth = $aPostParams['sAuth'];
            $sMethod = $aPostParams['sMethod'];
            if ($sMethod == 'update') {
                if ($sAuth == "login") {
                    if ($this->isUserLoggedIn()) {
                        if ($this->oModule->updateUserL($aPostParams, $_SESSION['iId'])) {
                            $aReturn['sCode'] = "200";
                            $aReturn['sMessage'] = "User data updated";
                            if (isset($aPostParams['sPassword'])) {
                                if ($aPostParams['sPassword'] != "") {
                                    unset($_SESSION['sEmail']);
                                    unset($_SESSION['iId']);
                                    $aReturn['sMessage'] = "User data updated, and you have been logged out";
                                }
                            }
                        } else {
                            $aReturn['sCode'] = "405";
                            $aReturn['sMessage'] = "User not updated, wrong parameters";
                        }
                    } else {
                        $aReturn['sCode'] = "405";
                        $aReturn['sMessage'] = "User not logged in";
                    }
                } else if ($sAuth == "api") {
                    if ($this->oModule->updateUserApi($aPostParams)) {
                        $aReturn['sCode'] = "200";
                        $aReturn['sMessage'] = "User data updated";
                    } else {
                        $aReturn['sCode'] = "405";
                        $aReturn['sMessage'] = "User not updated, wrong parameters";
                    }
                }
            } else {
                $aReturn['sCode'] = "405";
                $aReturn['sMessage'] = "Wrong Method";
            }
            return new JsonModel($aReturn);
        } else {
            $aReturn['sCode'] = "405";
            $aReturn['sMessage'] = "Wrong Method";
        }

        return new JsonModel($aReturn);
    }

    public function detailsAction()
    {
        $aReturn = [];

        if ($this->getRequest()->isPost()) {
            //retrieve params
            $aPostParams = $this->params()->fromPost();

            $aRules = [
                "sAuth" => 'Authentification',
                "sMethod" => 'Method',
            ];

            $bMissingParam = $this->validate($aPostParams, $aRules);

            if (!$bMissingParam) {
                return new JsonModel($this->aValidateResponse);
            }

            $sAuth = $aPostParams['sAuth'];
            $sMethod = $aPostParams['sMethod'];

            if ($sAuth == "api") {
                if ($sMethod == 'details') {
                    if ($this->isApiKeyCorrect($aPostParams)) {
                        $aResult = $this->oModule->fetchUser($aPostParams['iUserId']);
                        if ($aResult) {
                            $aReturn['sCode'] = "200";
                            $aReturn['aUserDetails'] = $aResult;
                        } else {
                            $aReturn['sCode'] = "404";
                            $aReturn['sMessage'] = "User does not exist";
                        }
                    } else {
                        $aReturn['sCode'] = "405";
                        $aReturn['sMessage'] = "Api key not correct";
                    }
                } else {
                    $aReturn['sCode'] = "405";
                    $aReturn['sMessage'] = "Method not supported";
                }
            } else {
                $aReturn['sCode'] = "405";
                $aReturn['sMessage'] = "Authentification method not supported";
            }
        } else {
            $aParams = $this->params()->fromRoute();
            if (isset($aParams['id'])) {
                if ($this->isUserLoggedIn()) {
                    $aResult = $this->oModule->fetchUser($aParams['id']);
                    if ($aResult) {
                        $aReturn['sCode'] = "200";
                        $aReturn['aUserDetails'] = $aResult;
                    } else {
                        $aReturn['sCode'] = "404";
                        $aReturn['sMessage'] = "User does not exist";
                    }

                } else {
                    $aReturn['sCode'] = "405";
                    $aReturn['sMessage'] = "User not logged in";
                }
            } else {
                $aReturn['sCode'] = "405";
                $aReturn['sMessage'] = "Missing Parameter - login";
            }

        }

        return new JsonModel($aReturn);
    }

    public function listAction()
    {
        $aReturn = [];

        if ($this->getRequest()->isPost()) {
            //retrieve params
            $aPostParams = $this->params()->fromPost();

            $aRules = [
                "sAuth" => 'Authentification',
                "sMethod" => 'Method',
            ];

            $bMissingParam = $this->validate($aPostParams, $aRules);

            if (!$bMissingParam) {
                return new JsonModel($this->aValidateResponse);
            }

            $sAuth = $aPostParams['sAuth'];
            $usMethod = $aPostParams['sMethod'];

            if ($sAuth == "api") {
                if ($usMethod == 'list') {
                    if ($this->isApiKeyCorrect($aPostParams)) {
                        $aResult = $this->oModule->fetchUserList();
                        if ($aResult) {
                            $aReturn['sCode'] = "200";
                            $aReturn['aUserDetails'] = $aResult;
                        } else {
                            $aReturn['sCode'] = "404";
                            $aReturn['sMessage'] = "User does not exist";
                        }
                    } else {
                        $aReturn['sCode'] = "405";
                        $aReturn['sMessage'] = "Api key not correct";
                    }
                } else {
                    $aReturn['sCode'] = "405";
                    $aReturn['sMessage'] = "Authentification method not supported";
                }
            }
            else{
                $aReturn['sCode'] = '405';
                $aReturn['sMessage'] = "Method not supported";
            }
        } else {


            if ($this->isUserLoggedIn()) {
                $aResult = $this->oModule->fetchUserList();
                $aReturn['sCode'] = "200";
                $aReturn['aUserDetails'] = $aResult;
            } else {
                $aReturn['sCode'] = "405";
                $aReturn['sMessage'] = "User not logged in";
            }


        }

        return new JsonModel($aReturn);
    }

    public function deleteAction()
    {

        $aReturn = [];

        if ($this->getRequest()->isPost()) {
            //retrieve params
            $aPostParams = $this->params()->fromPost();

            $aRules = [
                "sAuth" => 'Authentification',
                "sMethod" => 'Method',
            ];

            $bMissingParam = $this->validate($aPostParams, $aRules);

            if (!$bMissingParam) {
                return new JsonModel($this->aValidateResponse);
            }

            $sAuth = $aPostParams['sAuth'];
            $usMethod = $aPostParams['sMethod'];

            if ($sAuth == "api") {
                if ($usMethod == 'delete') {
                    if ($this->isApiKeyCorrect($aPostParams)) {
                        if ($this->oModule->removeUserAPI($aPostParams['iUserId'], $aPostParams['sApiKey'])) {
                            $aReturn['sCode'] = "200";
                            $aReturn['iDeletedUserId'] = $aPostParams['iUserId'];
                            $aReturn['sMessage'] = "User deleted";
                        } else {
                            $aReturn['sCode'] = "404";
                            $aReturn['sMessage'] = "User with that id does not exists or you are trying to delete yourself";
                        }
                    } else {
                        $aReturn['sCode'] = "405";
                        $aReturn['sMessage'] = "Api key not correct";
                    }
                } else {
                    $aReturn['sCode'] = "405";
                    $aReturn['sMessage'] = "Authentification method not supported";
                }
            }
            else{
                $aReturn['sCode'] = '405';
                $aReturn['sMessage'] = "Method not supported";
            }
        } else {
            $aParams = $this->params()->fromRoute();
            if (isset($aParams['id'])) {
                if ($this->isUserLoggedIn()) {
                    if ($aParams['id'] != $_SESSION['iId']) {
                        if ($this->oModule->removeUser($aParams['id'])) {
                            $aReturn['sCode'] = "200";
                            $aReturn['iDeletedUserId'] = $aParams['id'];
                            $aReturn['sMessage'] = "User deleted";
                        } else {
                            $aReturn['sCode'] = "404";
                            $aReturn['sMessage'] = "User with that id does not exists";
                        }
                    } else {
                        $aReturn['sCode'] = "405";
                        $aReturn['sMessage'] = "You cannot delete yourself";
                    }

                } else {
                    $aReturn['sCode'] = "405";
                    $aReturn['sMessage'] = "User not logged in";
                }
            } else {
                $aReturn['sCode'] = "405";
                $aReturn['sMessage'] = "Missing Parameter - id";
            }

        }

        return new JsonModel($aReturn);
    }

    /***********************FUNCTIONS***********************/
    public function loginUser($aParams)
    {
        $_SESSION['sEmail'] = $aParams['email'];
        $_SESSION['iId'] = $aParams['id'];
    }

    public function isUserLoggedIn()
    {
        if (isset($_SESSION['sEmail'])) {
            if (isset($_SESSION['iId'])) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function isApiKeyCorrect($aParams)
    {
        $aResults = $this->oModule->fetchAll("api_key = '" . $aParams['sApiKey'] . "'")->current();
        if ($aResults) {
            return true;
        } else {
            return false;
        }
    }

    public function validate($aParams, $aRules)
    {
        $bReturn = true;
        foreach ($aRules as $sKey => $sValue) {

            if (!isset($aParams[$sKey])) {
                $this->aValidateResponse['aMissingParams'][] = $sValue;
                $bReturn = false;
            } else if ($aParams[$sKey] == "") {
                $this->aValidateResponse['aMissingParams'][] = $sValue;
                $bReturn = false;
            }
        }

        if (!$bReturn) {
            $this->aValidateResponse['sResponseCode'] = "405";
            return false;
        } else {
            return true;
        }
    }
}